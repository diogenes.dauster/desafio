//
//  CarinhoViewController.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/27/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
        
    weak var cart: Cart?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}


extension CartViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        // retorna ao menos uma celula para o modelo de celula vazia
        // return at least 1 cell for the empty cell model
        
        if let count = cart?.items.count,
            count > 0 {
            return count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // se não tiver items usa o modelo de celula vazia se não use o customizado
        // if items variable doesn't have nothing, it use a empty cell if not a custom cell
        
        if let items = cart?.items,
        items.count > 0 {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath)
            
            if let cartCell = cell as? CartCell {
                cartCell.productNameLabel.text = "Produto: \(items[indexPath.row].product.title)"
                cartCell.productQuantityLabel.text = "Quantidade: \(items[indexPath.row].quantity)"

                let addicionais = items[indexPath.row].product.additional?.reduce("", { acumulator, object in
                    return acumulator + object.rawValue + " "
                })
                
                cartCell.productAdditionalLabel.text = "Adicionais: \(addicionais!)"
                cartCell.productImage.image =  UIImage(data:  items[indexPath.row].product.imgData)
                
                return cartCell
            }
            
            return cell
    
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            tableView.separatorStyle = .none
            
            return cell
            
        }
    }
    
    
}
