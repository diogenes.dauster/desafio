//
//  DetalheViewController.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/27/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import UIKit

protocol DetailViewControllerDelegate: class {
    func detailViewController(_ controller: DetailViewController,didFinishAdding item: Item)
}


class DetailViewController: UIViewController {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    
    @IBOutlet var productSizeCollection: [UIButton]!
    @IBOutlet var productSugarCollection: [UIButton]!
    @IBOutlet var productAdditionalCollection: [UIButton]!
    
    weak var delegate: DetailViewControllerDelegate?
    
    var product: Product? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  let productDetail = product {
            loadProduct(productDetail)
        }
        
    }
    
    
    @IBAction func selectSize(_ sender: Any) {
        if let button = sender as? UIButton {
            selectItem(item: button, from: productSizeCollection)
        }
    }
    
    
    @IBAction func selectSugar(_ sender: Any) {
        if let button = sender as? UIButton {
            selectItem(item: button, from: productSugarCollection)
        }
    }
    
    @IBAction func selectAdditionals(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.alpha == 1 {
                button.alpha = 0.5
            } else {
                button.alpha = 1
            }
        }
    }
    
    
    @IBAction func selectQuantity(_ sender: Any) {
        if let button = sender as? UIButton,
            let value = productQuantityLabel.text,
            let quantity = Int(value) {
            
            if button.tag == 1 {
                productQuantityLabel.text = String(quantity + 1)
            } else {
                if quantity - 1 >= 0 {
                    productQuantityLabel.text = String(quantity - 1)
                } else {
                    feedbackMessage(title: "Aviso", message: "Quantidade menor que zero")
                }
            }
        }
        
    }
    
    
    @IBAction func addItemToCart(_ sender: Any) {
        
        if var productCart =  product {
            let sugar  = getSelectedItens(productSugarCollection)[0].tag
            let additionals = getSelectedItens(productAdditionalCollection).map{
                ProductApi.getAdditionalByIndex($0.tag)
            }
            productCart.sugar = sugar
            productCart.additional = additionals
            
            if let size = Product.Size(rawValue: getSelectedItens(productSizeCollection)[0].tag) {
                productCart.size = size
            }
            
            if let value = productQuantityLabel.text,
                let quantity = Int(value){
                
                if quantity > 0 {
                    let item = Item(product: productCart, quantity: quantity )
                    delegate?.detailViewController(self, didFinishAdding: item)
                } else {
                    feedbackMessage(title: "Aviso", message: "Quantidade menor que zero")
                }
            }
        }
    }
    
    
    func getSelectedItens(_ items: [UIButton]) -> [UIButton] {
        return items.filter { $0.alpha == 1 }
    }
    
    func selectItem(item button: UIButton,from collection: [UIButton]) {
        
        collection.forEach { $0.alpha = 0.5 }
        button.alpha = 1
        
    }
    
    func feedbackMessage(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let item = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(item)
        self.present(alert, animated: true, completion: nil)
    }
    
    func loadProduct(_ product: Product) {
        
        productImage.image = UIImage(data: product.imgData)
        productTitleLabel.text = product.title
        
        selectSize(productSizeCollection.filter { $0.tag == product.size.rawValue }[0])
        selectSugar(productSugarCollection.filter { $0.tag == product.sugar }[0])
        
        
        if var additionals = product.additional  {
            
//            var selectedAddtionals:[UIButton] = []
            
            productAdditionalCollection.forEach{ button in
                
                for (index,additional) in additionals.enumerated() {
                    
                    if ProductApi.getIndexByAdditional(additional) == button.tag {
                        button.isHidden = false
                        additionals.remove(at: index)
                        //selectedAddtionals.append(button)
                        break
                    }
                }
            }
            
//            selectedAddtionals.forEach{
//                selectAdditionals($0)
//            }
            
        }
        
        
        
    }
    
    
}
