//
//  ItemTableViewCell.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/30/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productAdditionalLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
