//
//  ViewController.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/25/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    var menuProducts: Products = Products()
    var cart: Cart = Cart()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                    
        
        let produtoApi = ProductApi()
        produtoApi.getProductsFromWeb { sender in            
            if let products = sender as? Products {
                self.menuProducts = products
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            }
        }
                
//        do {
//            menuProducts =  try ProductApi.getProductsFromWeb()
//        } catch {
//            print(error)
//        }
                
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DetailSegue" {
            if let detailViewController = segue.destination as? DetailViewController,
                let product = sender as? Product {
                detailViewController.product = product
                detailViewController.delegate = self
            }
        }
        
        if segue.identifier == "CartSegue" {
            if let cartViewController = segue.destination as? CartViewController {
                cartViewController.cart = self.cart
            }
        }
        
    }

}


extension MenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuProducts.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath)
        
        if let productCell = cell as? ProductCell{
            let product = menuProducts.products[indexPath.row]
            
            productCell.productImage.image = UIImage(data: product.imgData )
            productCell.productTitleLabel.text = product.title
            
            return productCell
        }
        
        return cell
    }
    
}

extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "DetailSegue", sender: menuProducts.products[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}


extension MenuViewController: DetailViewControllerDelegate {
    func detailViewController(_ controller: DetailViewController, didFinishAdding item: Item) {
        cart.items.append(item)
        controller.navigationController?.popViewController(animated: true)
    }
}
