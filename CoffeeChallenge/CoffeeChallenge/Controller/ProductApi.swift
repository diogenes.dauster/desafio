//
//  ProdutoApi.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/27/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import Foundation

class ProductApi {
    
    enum DecodingError : Error {
        case missingFile
    }
    
    func getProductsFromWeb(completion: @escaping ((AnyObject) -> Void)) {
        
        if let url = URL(string: "https://desafio-mobility.herokuapp.com/products.json") {
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data {
                    
                    do {
                        let jsonDecoder =  JSONDecoder()
                        let products = try jsonDecoder.decode(Products.self, from: data)
                        
                        DispatchQueue.main.async {
                            completion(products)
                        }
                        
                    } catch let error {
                        print(error)
                    }
                }
            }.resume()
        }
        
    }
    
    func getProductsFromFile() throws -> Products {
                        
        guard let jsonUrl = Bundle.main.url(forResource: "products", withExtension: "json")  else {
            throw ProductApi.DecodingError.missingFile
        }
        
        let jsonDecoder =  JSONDecoder()
        let data = try Data(contentsOf: jsonUrl)
        let products = try jsonDecoder.decode(Products.self, from: data)
        
        return products
        
    }
    
    static func getAdditionalByIndex(_ index: Int) -> Product.Additional {
        
        switch index {
        case 0:
            return Product.Additional.chantilly
        case 1:
            return Product.Additional.cinnamon
        case 2:
            return Product.Additional.milk
        case 3:
            return Product.Additional.coffee
        default:
            return Product.Additional.chocolate
        }
    }
    
    static func getIndexByAdditional(_ addtional: Product.Additional) -> Int {
        
        switch addtional {
        case .chantilly:
            return 0
        case .cinnamon:
            return 1
        case .milk:
            return 2
        case .coffee:
            return 3
        case .chocolate:
            return 4
        }
        
    }
    
}
