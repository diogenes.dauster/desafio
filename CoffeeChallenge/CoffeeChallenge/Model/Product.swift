//
//  Product.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/27/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import Foundation


struct Product: Decodable {
    
    enum Size: Int, Decodable {
        case small = 0
        case medium
        case large
    }
    
    enum Additional: String, Decodable {
        case coffee
        case chocolate
        case cinnamon
        case milk
        case chantilly
    }
        
    let title: String
    let image: String
    var size: Size
    var sugar: Int
    var additional: [Additional]?
    var imgData: Data {
        String.base64Convert(base64String: image)!
    }
    
    init(title: String, image: String, size: Size,sugar: Int, additional: [Additional]?) {
        self.title = title
        self.image = image
        self.size = size
        self.sugar = sugar
        self.additional = additional
    }
    
}


class Products: Decodable {
    var products: [Product] = []
}


// Retira 'data:image/png;base64,'  da string para fazer o cast para tipo Data
// change the expression 'data:image/png;base64,' for blank for making the cast to the type Data.
extension String {

    static func base64Convert(base64String: String?) -> Data? {
           if (base64String?.isEmpty)! {
               return nil
           }else {
               // !!! Separation part is optional, depends on your Base64String !!!
               let temp = base64String?.components(separatedBy: ",")
               let dataDecoded : Data = Data(base64Encoded: temp![1], options: .ignoreUnknownCharacters)!
               return dataDecoded
           }
    }
}
