//
//  Item.swift
//  CoffeeChallenge
//
//  Created by Diógenes Dauster on 9/30/19.
//  Copyright © 2019 Dauster. All rights reserved.
//

import Foundation


struct Item {
    let product: Product
    let quantity: Int
    
    init(product: Product, quantity: Int) {
        self.product = product
        self.quantity = quantity
    }
}
